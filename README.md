# Test

## Introduccion

Por cuestiones contables el cliente nos pide hacer un mini sistema donde pueda controlar el día a día de su equipo.

El cliente requiere tener un panel para poder ver sus empleados y que sus trabajadores puedan realizar reportes de lo sucedido en el dia.


## Instrucciones

Crear un proyecto pequeño que incluya un poco de todas las areas de desarrollo web.

### Backend

/// Realizar en test en laravel
La estructura de la base de datos queda a su criterio

## Front

Crear una vista usando BOOSTRAP y JS puro*, AJAX*, Angular* o con lo que se sienta comodo usando.
*usar solo uno

1. agregar usuario: 
    Formulario con los datos a llenar. SIN REFRESCAR (por eso el uso de JS)
    Alertas de OK or Error
    
2. editar usuario: 
    Formulario con los datos a llenar. SIN REFRESCAR (por eso el uso de JS)
    Alertas de OK or Error
    
3. Listar usuarios
    Listar en una table todos los usuarios en la ultima columna poner los botones de accion de EDITAR y BORRAR 
    
4. delete usuario:
    Al precionar borrar ese usuario de la vista SIN REFRESCAR (por eso el uso de JS)


## Subir a este repositorio
    * IMPORTANTE Crear una nueva RAMA *
    El nombre de la rama debe ser su nombre
    ****** NO SUBIR EN MASTER **********

